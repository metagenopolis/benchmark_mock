Assemblies                                           scaffolds  scaffolds_broken
Acidilobus_saccharovorans_345_15                     -          -               
Acidobacterium_capsulatum_ATCC_51196                 766030     742865          
Aciduliprofundum_boonei_T469                         -          -               
Akkermansia_muciniphila_ATCC_BAA_835                 2365373    2344560         
Archaeoglobus_fulgidus_DSM_4304                      1325331    1295871         
Bacteroides_thetaiotaomicron_VPI_5482                353841     341745          
Bacteroides_vulgatus_ATCC_8482                       2509840    2442964         
Bordetella_bronchiseptica_RB50                       11986      11986           
Caldicellulosiruptor_bescii_DSM_6725                 1104935    1080712         
Caldicellulosiruptor_saccharolyticus_DSM_8903        2817833    2817633         
Chlorobium_limicola_DSM_245                          2695742    2694433         
Chlorobium_phaeobacteroides_DSM_266                  2831152    2826767         
Chlorobium_phaeovibrioides_DSM_265                   1950574    1949964         
Chlorobium_tepidum_TLS                               2119593    2118014         
Chloroflexus_aurantiacus_J_10_fl                     4007995    3954265         
Deinococcus_radiodurans_R1                           130665     127501          
Desulfitobacterium_dehalogenans_ATCC_51507           -          -               
Desulfobulbus_oralis_sp._ONRL                        2684631    2683632         
Desulfovibrio_desulfuricans_ND132                    -          -               
Desulfovibrio_piger_ATCC_29098                       2257261    2233890         
Desulfovibrio_vulgaris_Hildenborough                 -          -               
Dictyoglomus_turgidum_DSM_6724                       61208      60014           
Enterococcus_faecalis_OG1RF                          3988       3988            
Fusobacterium_nucleatum_subsp._nucleatum_ATCC_25586  2115637    2115159         
Gemmatimonas_aurantiaca_T_27                         4634300    4633992         
Geobacter_bemidjiensis_Bem                           -          -               
Geobacter_sulfurreducens_PCA                         1119836    1089044         
Halobacterium_salinarum_NRC_1                        2345221    2345041         
Haloferax_volcanii_DS2                               -          -               
Herpetosiphon_aurantiacus_DSM_785                    6685597    6683967         
Hydrogenobaculum_sp._Y04AAS1                         1501830    1499423         
Ignicoccus_hospitalis_KIN4_I                         1295896    1295796         
Ignicoccus_islandicus_DSM_13165                      1379010    1377214         
Leptothrix_cholodnii_SP_6                            -          -               
Methanobrevibacter_oralis_DSM_7256                   2055873    2054774         
Methanocaldococcus_jannaschii_DSM_2661               1730471    1730371         
Methanococcus_maripaludis_C5                         1775936    1774625         
Methanococcus_maripaludis_S2                         576045     566614          
Methanomassiliicoccus_luminyensis_B10                -          -               
Methanomethylovorans_hollandica_DSM_15978            771        771             
Methanopyrus_kandleri_AV19                           1296694    1274817         
Methanosarcina_acetivorans_C2A                       -          -               
Methanothermus_fervidus_DSM_2088                     1228859    1228859         
Nanoarchaeum_equitans_Kin4_M                         424746     423527          
Nitrosomonas_europaea_ATCC_19718                     2707304    2707104         
Nostoc_sp._PCC_7120                                  5727276    5660905         
Paraburkholderia_xenovorans_LB400                    2108548    2045316         
Persephonella_marina_EX_H1                           1976554    1976554         
Porphyromonas_gingivalis_ATCC_33277                  2202707    2202379         
Pseudomonas_fluorescens_SBW25                        6361741    6327509         
Pseudomonas_putida_KT2440                            5438157    5387182         
Pyrobaculum_aerophilum_IM2                           7498       7498            
Pyrobaculum_arsenaticum_DSM_13514                    -          -               
Pyrobaculum_calidifontis_JCM_11548                   1814400    1798270         
Pyrococcus_furiosus_DSM_3638                         3659       3659            
Pyrococcus_horikoshii_OT3                            1732186    1732086         
Rhodopirellula_baltica_SH_1                          7059302    7057716         
Ruegeria_pomeroyi_DSS_3                              575        575             
Ruminiclostridium_thermocellum_ATCC_27405            878630     852823          
Salinispora_arenicola_CNS_205                        243524     236161          
Salinispora_tropica_CNB_440                          32437      31855           
Shewanella_baltica_OS185                             2587512    2577753         
Shewanella_baltica_OS223                             4877025    4871175         
Shewanella_loihica_PV_4                              1199753    1160514         
Sulfolobus_tokodaii_str._7                           1665       1665            
Sulfurihydrogenibium_sp._YO3AOP1                     888364     868691          
Thermoanaerobacter_pseudethanolicus_ATCC_33223       2237248    2237148         
Thermotoga_neapolitana_DSM_4359                      1874617    1874317         
Thermotoga_petrophila_RKU_1                          1800246    1800046         
Treponema_denticola_ATCC_35405                       2348729    2320089         
Wolinella_succinogenes_DSM_1740                      1797942    1780412         
not_aligned                                          505472     426856          
