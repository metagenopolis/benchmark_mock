#!/usr/bin/bash

#for minion
guppy_basecaller --flowcell FLO-MIN106 --kit SQK-LSK109 --fast5subdir MOCK --qscore_filtering --disable_pings -s MOCK_temp --num_callers 1 --cpu_threads_per_caller 12
guppy_barcoder --input_path MOCK_temp --barcode_kits EXP-NBD103 --worker_threads 12 --save_path MOCK_demultiplex

