#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html

"""Create protein marker tree from genome protein fasta file"""

import argparse
import os
import sys
import subprocess
import time
import math
import re

__author__ = "Mathieu Almeida"
__copyright__ = "Copyright 2022, INRAE"
__license__ = "GPL"
__version__ = "1.1.0"
__maintainer__ = "Mathieu Almeida"
__email__ = "mathieu.almeida@inrae.fr"
__status__ = "Developpement"

COG_list = ['COG0012','COG0016','COG0018','COG0048','COG0049','COG0052',
            'COG0080','COG0081','COG0087','COG0088','COG0090','COG0091',
            'COG0092','COG0093','COG0094','COG0096','COG0097','COG0098',
            'COG0099','COG0100','COG0102','COG0103','COG0124','COG0172',
            'COG0184','COG0185','COG0186','COG0197','COG0200','COG0201',
            'COG0202','COG0215','COG0256','COG0495','COG0522','COG0525',
            'COG0533','COG0541','COG0552']

                      
def isfile(path):
    """Check if path is an existing file.
      :Parameters:
          path: Path to the file
    """
    if not os.path.isfile(path):
        if os.path.isdir(path):
            msg = "{0} is a directory".format(path)
        else:
            msg = "{0} does not exist.".format(path)
        raise argparse.ArgumentTypeError(msg)
    return path


def isdir(path):
    """Check if path is an existing file.
      :Parameters:
          path: Path to the file
    """
    if not os.path.isdir(path):
        if os.path.isfile(path):
            msg = "{0} is a file.".format(path)
            raise argparse.ArgumentTypeError(msg)
        else:
            msg = "{0} does not exist.".format(path)        
    
    return path


def config_parameters():
    """Extract program options
    """
    parser = argparse.ArgumentParser(description=__doc__,
                            usage="{0} -h [options] [arg]".format(sys.argv[0]))
    parser.add_argument('-f', '--folder_path', 
                        dest='folder_path', type=isdir, help='Folder Path'
                        ' containing the protein faa files')  
    parser.add_argument('-o', '--output_tree', default='tree', 
                        dest='output_tree', type=str, help='Tree creation')  
    parser.add_argument('-r', dest='result_dir', type=isdir,
                        default=os.curdir + os.sep, help='Path to result '
                        'directory.')
    parser.add_argument('-t', dest='num_threads', type=int,
                        default=1,help='Number of cpus for the process') 
    return parser.parse_args()


def extract_list_genomes(path_genome):
   protein_files=[]
   for file in os.listdir(path_genome):
      if file.endswith(".faa"):
         protein_files.append(file)   

   return protein_files
   
   
def run_fetchMG(genome_name, protein_file, result_dir, num_threads):
   """extract the 40 marker protein for each genome
   """
   marker_folder = result_dir + '/' + genome_name + '_marker/'

   #if not os.path.exists(marker_folder): 
   #    os.makedirs(marker_folder)   

   if not os.path.isfile(marker_folder+"/COG0012.faa"):
      print(genome_name + " extract marker...")
      fetch_cmd = "fetchMGs.pl -m extraction {0} " \
               "-o {1} -p -t {2}".format( 
               protein_file, marker_folder, num_threads)
      os.system(fetch_cmd)

   os.system("rm -rf " + marker_folder + "/hmmResults/")
   os.system("rm -rf " + marker_folder + "/temp/")
            
      
def create_COG_file(result_dir, list_protein_file):
   """create COG protein file that include the input genome and the reference
      if requested
   """
   
   #go through reference and extract protein from list ref
   for COG in COG_list: 
      if not os.path.isfile(result_dir + '/alignment/' + COG + '.faa'):
         print(COG + " protein file creation...")
         #go through input marker and add them to COG file       
         with open(result_dir + '/alignment/' + COG + '.faa', 'wt') as COG_file:
            
            #add cog from genome to compare
            for protein_file in list_protein_file:
               genome_name = ".".join(protein_file.split('.')[:-1])
               COG_file_name = result_dir+'/marker/'+genome_name+'_marker/'+COG+'.faa'
                        
               if os.path.isfile(COG_file_name):
               
                  #if only one marker (no contaminant or not two fragmented partial)
                  grep_cmd = "grep -c '>' {0}".format(COG_file_name)
      
                  process_grep = subprocess.Popen(grep_cmd, shell=True,
                                            stdout=subprocess.PIPE)              
                                            
                  nb_COG = int(process_grep.communicate()[0])
            
                  if nb_COG == 1:                                              
                     with open(COG_file_name,'rt') as genome_prot:   
                        for line in genome_prot:                  
                           if line[0]=='>':                     
                              COG_file.write('>' + genome_name + '\n') 
                           else:
                              COG_file.write(line.strip().replace('*','') + '\n')   
               
            
            
def start_MUSCLE_COG_alignment(result_dir):   
   """Aligned individually COG using MUSCLE protein
   """
   for COG in COG_list:
      if not os.path.isfile(result_dir + '/alignment/' + COG + '.align.faa'):
         print(COG + ' alignment...')
         muscle_cmd = "muscle -in {0}/{1} "\
                      " -out {0}/{2} -quiet".format(
                      result_dir + '/alignment/', 
                      COG + '.faa', COG + '.align.faa') 
         os.system(muscle_cmd)     

      
def merge_aligned_marker(result_dir, list_protein_files):
   """Merge the 40 marker protein into one. Fill by gap for absent protein
      Remove genomes with less than 5 markers
   """
   marker = {}
   COG_size = {}
   ref_merged_prot={}
   ref_marker_count={}

   for COG in COG_list:
      with open(result_dir + '/alignment/' + COG + '.align.faa', 'rt') as marker_file:
         header= ''
         prot = ''
         for line in marker_file:
            if line[0]=='>':
               if header !='':
                  ref_id = header[1:].strip()
                  marker[ref_id, COG] = prot
                  
               header= line.strip()
               prot=''
            else:
               prot = prot + line.strip()
         #last line
         ref_id = header[1:].strip()
         marker[ref_id, COG] = prot
         COG_size[COG] = len(prot)
   
   #init and count ref
   new_list_ref=[]
   for protein_file in list_protein_files:
      ref = ".".join(protein_file.split('.')[:-1])
      
      ref_marker_count[ref] = 0
      ref_merged_prot[ref] = ''
      for COG in COG_list:
         prot = marker.get((ref, COG))
         if prot is not None:
            ref_marker_count[ref] = ref_marker_count[ref] + 1
      if ref_marker_count[ref] >= 5:
         new_list_ref.append(ref)
           
   for COG in COG_list:
      for ref in new_list_ref:
         prot = marker.get((ref, COG)) 
         
         if prot is not None:
            ref_merged_prot[ref] = ref_merged_prot[ref] + prot
         else:
            ref_merged_prot[ref] = ref_merged_prot[ref] + '-'*COG_size[COG]
         
   return ref_merged_prot, new_list_ref


def create_tree_file(merged_marker_list, result_dir, num_threads, output_tree,
                     list_ref):
   """create Newick file for tree
   """
   full_ref = ''                  
   list_genome = []

   with open(result_dir + '/tree/alignment.fasta', 'wt') as merged_alignment_file:
      for ref in list_ref:
         
         merged_alignment_file.write('>' + ref + '\n')
         merged_alignment_file.write(merged_marker_list[ref]+ '\n')
         list_genome.append(ref)

   FastTree_cmd = "FastTree -quiet -gamma -pseudo -spr 4 -mlacc 3 -slownni"\
       " {0}/{1} > {0}/{2}.newick".format(result_dir + '/tree/',
       'alignment.fasta', output_tree)   
   os.system(FastTree_cmd)   
   

def main():
    """Main program function
    """
    
    #init
    start_time = time.time()

    #Extract arguments
    args = config_parameters()
            
    full_path_result_dir = os.path.abspath(args.result_dir)
    full_path_genome_list = os.path.abspath(args.folder_path)  

    list_protein_files = extract_list_genomes(full_path_genome_list)
    
    #Create output folder if it doesn't exist
    if not os.path.exists(full_path_result_dir):             
       os.makedirs(full_path_result_dir)         
 
    if not os.path.exists(full_path_result_dir + '/marker/'): 
          os.makedirs(full_path_result_dir + '/marker/') 

    print ('extract marker proteins')
    for protein_file in list_protein_files:
       genome_name = ".".join(protein_file.split('.')[:-1])

       full_path_protein_file = full_path_genome_list + '/' + protein_file

       run_fetchMG(genome_name, full_path_protein_file, 
                   full_path_result_dir + '/marker/',  args.num_threads)
              
    print ('create COG file for alignment')
    if not os.path.exists(args.result_dir + '/alignment/'): 
       os.makedirs(args.result_dir + '/alignment/')  

    
    print ('create COG file')    
    create_COG_file(full_path_result_dir, list_protein_files)
    
    
    print ('start muscle COG alignment using MUSCLE')
    start_MUSCLE_COG_alignment(full_path_result_dir)
      
    print ('merge aligned result')
    merged_marker_list, new_list_ref = merge_aligned_marker(full_path_result_dir,
                                                       list_protein_files)
            
    print ('calculate phylogenetic distance between the marker protein')  
    if not os.path.exists(args.result_dir + '/tree/'): 
       os.makedirs(args.result_dir + '/tree/')  


    print ('create tree')
    create_tree_file(merged_marker_list, full_path_result_dir, args.num_threads,
                     args.output_tree, new_list_ref)
      
    stop_time = time.time()     
    diff_tuple = time.gmtime(stop_time - start_time)  
    print ('end in {0}h:{1}m:{2}s end'.format(diff_tuple.tm_hour, 
                                              diff_tuple.tm_min, 
                                              diff_tuple.tm_sec))
                                        
                                                                   
if __name__ == '__main__':
    main()
                                   
