#!/usr/bin/bash

sample="MOCK_001"

#for illumina 
fastp --in1 ${sample}_1.raw.fastq --in2 ${sample}_2.raw.fastq --cut_front --cut_tail --adapter_sequence 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCA' --adapter_sequence_r2 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT' \
 --n_base_limit 0 --length_required 45 --out1 ${sample}_1.fastq --out2 ${sample}_2.fastq --thread 12 --html ${sample}.fastp.html --json ${sample}.fastp.json 

#for dnbseq G400/T7
fastp --in1 ${sample}_1.raw.fastq --in2 ${sample}_2.raw.fastq --cut_front --cut_tail --adapter_sequence 'AAGTCGGAGGCCAAGCGGTCTTAGGAAGACAA' --adapter_sequence_r2 'AAGTCGGATCGTAGCCATGTCGTTCTGTGAGCCAAGGAGTTG' \
 --n_base_limit 0 --length_required 45 --out1 ${sample}_1.fastq --out2 ${sample}_2.fastq --thread 12 --html ${sample}.fastp.html --json ${sample}.fastp.json

#for proton/s5
AlienTrimmer.jar -c alientrimmer_contaminants.tfa -k 10 -l 45 -m 5 -p 40 -q 20 -i ${sample}.raw.fastq -o ${sample}.fastq

#for pacbio hifi reads already quality checked after sequencing

#for minion see basecalling for minion preprocessing
