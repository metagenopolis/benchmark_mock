#!/usr/bin/bash

sample="MOCK_001"
techno="illumina"

metaquast.py --min-alignment 500 --fragmented --min-identity 97 --split-scaffolds --threads 12 --min-contig 500 --circos -r ${sample} ${sample}.${techno}.fasta -o metaquast_${sample}.${techno}

