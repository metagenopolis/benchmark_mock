Assemblies                                           scaffolds  scaffolds_broken
Acidilobus_saccharovorans_345_15                     -          -               
Acidobacterium_capsulatum_ATCC_51196                 19.57      19.57           
Aciduliprofundum_boonei_T469                         194.55     194.55          
Akkermansia_muciniphila_ATCC_BAA_835                 51.94      51.94           
Archaeoglobus_fulgidus_DSM_4304                      23.80      23.80           
Bacteroides_thetaiotaomicron_VPI_5482                69.17      69.17           
Bacteroides_vulgatus_ATCC_8482                       79.62      79.62           
Bordetella_bronchiseptica_RB50                       77.41      77.44           
Caldicellulosiruptor_bescii_DSM_6725                 48.04      48.04           
Caldicellulosiruptor_saccharolyticus_DSM_8903        23.40      24.14           
Chlorobium_limicola_DSM_245                          27.14      26.66           
Chlorobium_phaeobacteroides_DSM_266                  79.47      79.47           
Chlorobium_phaeovibrioides_DSM_265                   25.51      25.55           
Chlorobium_tepidum_TLS                               13.54      13.54           
Chloroflexus_aurantiacus_J_10_fl                     32.97      32.97           
Deinococcus_radiodurans_R1                           34.76      34.76           
Desulfitobacterium_dehalogenans_ATCC_51507           -          -               
Desulfobulbus_oralis_sp._ONRL                        21.36      21.36           
Desulfovibrio_desulfuricans_ND132                    -          -               
Desulfovibrio_piger_ATCC_29098                       38.80      38.80           
Desulfovibrio_vulgaris_Hildenborough                 -          -               
Dictyoglomus_turgidum_DSM_6724                       33.03      33.03           
Enterococcus_faecalis_OG1RF                          40.97      40.97           
Fusobacterium_nucleatum_subsp._nucleatum_ATCC_25586  13.15      13.15           
Gemmatimonas_aurantiaca_T_27                         0.63       0.63            
Geobacter_bemidjiensis_Bem                           -          -               
Geobacter_sulfurreducens_PCA                         15.40      15.40           
Halobacterium_salinarum_NRC_1                        13.03      13.03           
Haloferax_volcanii_DS2                               -          -               
Herpetosiphon_aurantiacus_DSM_785                    18.57      18.57           
Hydrogenobaculum_sp._Y04AAS1                         28.46      28.46           
Ignicoccus_hospitalis_KIN4_I                         7.95       7.95            
Ignicoccus_islandicus_DSM_13165                      11.05      11.05           
Leptothrix_cholodnii_SP_6                            564.06     564.06          
Methanobrevibacter_oralis_DSM_7256                   49.78      51.00           
Methanocaldococcus_jannaschii_DSM_2661               3.36       3.36            
Methanococcus_maripaludis_C5                         244.82     244.82          
Methanococcus_maripaludis_S2                         51.81      51.81           
Methanomassiliicoccus_luminyensis_B10                0.00       0.00            
Methanomethylovorans_hollandica_DSM_15978            -          -               
Methanopyrus_kandleri_AV19                           29.81      29.81           
Methanosarcina_acetivorans_C2A                       85.32      85.32           
Methanothermus_fervidus_DSM_2088                     1.87       1.87            
Nanoarchaeum_equitans_Kin4_M                         6.96       6.96            
Nitrosomonas_europaea_ATCC_19718                     4.27       4.27            
Nostoc_sp._PCC_7120                                  24.12      24.12           
Paraburkholderia_xenovorans_LB400                    70.59      70.59           
Persephonella_marina_EX_H1                           1.57       1.57            
Porphyromonas_gingivalis_ATCC_33277                  12.11      12.06           
Pseudomonas_fluorescens_SBW25                        175.09     175.09          
Pseudomonas_putida_KT2440                            163.92     163.92          
Pyrobaculum_aerophilum_IM2                           93.21      93.21           
Pyrobaculum_arsenaticum_DSM_13514                    841.75     841.75          
Pyrobaculum_calidifontis_JCM_11548                   17.53      17.53           
Pyrococcus_furiosus_DSM_3638                         32.05      32.05           
Pyrococcus_horikoshii_OT3                            2.19       2.19            
Rhodopirellula_baltica_SH_1                          7.81       7.81            
Ruegeria_pomeroyi_DSS_3                              0.00       0.00            
Ruminiclostridium_thermocellum_ATCC_27405            55.74      55.74           
Salinispora_arenicola_CNS_205                        595.82     595.82          
Salinispora_tropica_CNB_440                          882.94     882.94          
Shewanella_baltica_OS185                             1589.68    1589.13         
Shewanella_baltica_OS223                             562.67     561.17          
Shewanella_loihica_PV_4                              17.92      17.92           
Sulfolobus_tokodaii_str._7                           472.71     472.71          
Sulfurihydrogenibium_sp._YO3AOP1                     39.20      39.20           
Thermoanaerobacter_pseudethanolicus_ATCC_33223       7.99       7.99            
Thermotoga_neapolitana_DSM_4359                      2.42       2.42            
Thermotoga_petrophila_RKU_1                          1.68       1.68            
Treponema_denticola_ATCC_35405                       30.74      30.74           
Wolinella_succinogenes_DSM_1740                      4.33       4.33            
