Assemblies                                           scaffolds  scaffolds_broken
Acidilobus_saccharovorans_345_15                     -          -               
Acidobacterium_capsulatum_ATCC_51196                 -          -               
Aciduliprofundum_boonei_T469                         -          -               
Akkermansia_muciniphila_ATCC_BAA_835                 191        191             
Archaeoglobus_fulgidus_DSM_4304                      351        351             
Bacteroides_thetaiotaomicron_VPI_5482                -          -               
Bacteroides_vulgatus_ATCC_8482                       1230       1230            
Bordetella_bronchiseptica_RB50                       -          -               
Caldicellulosiruptor_bescii_DSM_6725                 -          -               
Caldicellulosiruptor_saccharolyticus_DSM_8903        21         21              
Chlorobium_limicola_DSM_245                          19         19              
Chlorobium_phaeobacteroides_DSM_266                  106        106             
Chlorobium_phaeovibrioides_DSM_265                   7          7               
Chlorobium_tepidum_TLS                               62         62              
Chloroflexus_aurantiacus_J_10_fl                     574        574             
Deinococcus_radiodurans_R1                           -          -               
Desulfitobacterium_dehalogenans_ATCC_51507           -          -               
Desulfobulbus_oralis_sp._ONRL                        49         49              
Desulfovibrio_desulfuricans_ND132                    -          -               
Desulfovibrio_piger_ATCC_29098                       535        535             
Desulfovibrio_vulgaris_Hildenborough                 -          -               
Dictyoglomus_turgidum_DSM_6724                       -          -               
Enterococcus_faecalis_OG1RF                          -          -               
Fusobacterium_nucleatum_subsp._nucleatum_ATCC_25586  107        107             
Gemmatimonas_aurantiaca_T_27                         52         52              
Geobacter_bemidjiensis_Bem                           -          -               
Geobacter_sulfurreducens_PCA                         1551       1551            
Halobacterium_salinarum_NRC_1                        360        360             
Haloferax_volcanii_DS2                               -          -               
Herpetosiphon_aurantiacus_DSM_785                    37         37              
Hydrogenobaculum_sp._Y04AAS1                         52         52              
Ignicoccus_hospitalis_KIN4_I                         19         19              
Ignicoccus_islandicus_DSM_13165                      12         12              
Leptothrix_cholodnii_SP_6                            -          -               
Methanobrevibacter_oralis_DSM_7256                   61         61              
Methanocaldococcus_jannaschii_DSM_2661               12         12              
Methanococcus_maripaludis_C5                         55         55              
Methanococcus_maripaludis_S2                         -          -               
Methanomassiliicoccus_luminyensis_B10                -          -               
Methanomethylovorans_hollandica_DSM_15978            -          -               
Methanopyrus_kandleri_AV19                           179        179             
Methanosarcina_acetivorans_C2A                       -          -               
Methanothermus_fervidus_DSM_2088                     9          9               
Nanoarchaeum_equitans_Kin4_M                         25         25              
Nitrosomonas_europaea_ATCC_19718                     21         21              
Nostoc_sp._PCC_7120                                  672        672             
Paraburkholderia_xenovorans_LB400                    -          -               
Persephonella_marina_EX_H1                           4          4               
Porphyromonas_gingivalis_ATCC_33277                  28         28              
Pseudomonas_fluorescens_SBW25                        392        392             
Pseudomonas_putida_KT2440                            595        595             
Pyrobaculum_aerophilum_IM2                           -          -               
Pyrobaculum_arsenaticum_DSM_13514                    -          -               
Pyrobaculum_calidifontis_JCM_11548                   154        154             
Pyrococcus_furiosus_DSM_3638                         -          -               
Pyrococcus_horikoshii_OT3                            4          4               
Rhodopirellula_baltica_SH_1                          31         31              
Ruegeria_pomeroyi_DSS_3                              -          -               
Ruminiclostridium_thermocellum_ATCC_27405            -          -               
Salinispora_arenicola_CNS_205                        -          -               
Salinispora_tropica_CNB_440                          -          -               
Shewanella_baltica_OS185                             129        129             
Shewanella_baltica_OS223                             53         53              
Shewanella_loihica_PV_4                              1924       1924            
Sulfolobus_tokodaii_str._7                           -          -               
Sulfurihydrogenibium_sp._YO3AOP1                     661        661             
Thermoanaerobacter_pseudethanolicus_ATCC_33223       13         13              
Thermotoga_neapolitana_DSM_4359                      7          7               
Thermotoga_petrophila_RKU_1                          9          9               
Treponema_denticola_ATCC_35405                       241        241             
Wolinella_succinogenes_DSM_1740                      136        136             
